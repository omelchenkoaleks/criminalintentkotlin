package com.omelchenkoaleks.criminalintent.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * Класс сущности определяет структуру таблиц базы данных.
 * В этой таблице будет четыре столбца: id, title, date, isSolved
 */
@Entity
data class Crime(
    @PrimaryKey val id: UUID = UUID.randomUUID(), // простой способ генерирования уникальных идентификаторов
    var title: String = "",
    var date: Date = Date(), // такая инициализация задает по умолчанию текущую дату
    var isSolved: Boolean = false,
    var suspect: String = ""
) {
    val photoFileName
        get() = "IMG_$id.jpg" // имя файла будет уникальным
}