package com.omelchenkoaleks.criminalintent.crime_list

import androidx.lifecycle.ViewModel
import com.omelchenkoaleks.criminalintent.CrimeRepository
import com.omelchenkoaleks.criminalintent.model.Crime


class CrimeListViewModel: ViewModel() {

    private val crimeRepository = CrimeRepository.get()

    val crimeListLiveData = crimeRepository.getCrimes()

    fun addCrime(crime: Crime) {
        crimeRepository.addCrime(crime)
    }

}