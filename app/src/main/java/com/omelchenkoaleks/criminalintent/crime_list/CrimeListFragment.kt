package com.omelchenkoaleks.criminalintent.crime_list

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.omelchenkoaleks.criminalintent.R
import com.omelchenkoaleks.criminalintent.model.Crime
import java.util.*

private const val TAG = "CrimeListFragment"

class CrimeListFragment : Fragment() {

    /**
     * Интерфейс обратного вызова, будет использоваться для того, чтобы фрагмент мог вызывать функции,
     * связанные с его хост-activity, без необходимости знать что-либо о том, какая activity является хостом.
     *
     * То есть этот интерфейс определяет работу (делегирует), которую должна выполнить хост-activity.
     */
    interface Callbacks {
        fun onCrimeSelected(crimeId: UUID)
    }

    /**
     * Свойство, чтобы удерживать объект. реализующий Callbacks.
     */
    private var callbacks: Callbacks? = null

    private lateinit var crimeRecyclerView: RecyclerView
    private var adapter: CrimeAdapter? = CrimeAdapter(emptyList())

    private val crimeListViewModel: CrimeListViewModel by lazy {
        ViewModelProvider(this).get(CrimeListViewModel::class.java)
    }

    // Переопределен для установки свойства callbacks
    override fun onAttach(context: Context) {
        super.onAttach(context)
        callbacks = context as Callbacks?
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true) // Сообщаем FragmentManager, что экземпляр CrimeListFragment должен получать обратные вызовы меню.
    }

    companion object {

        // Чтобы Activity могли получить экзмпляр этого фрагмента
        fun newInstance(): CrimeListFragment {
            return CrimeListFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_crime_list, container, false)
        crimeRecyclerView =
            view.findViewById(R.id.crime_recycler_view) as RecyclerView

        // RecyclerView не отображает элементы на самом экране. Эту задачу он передает объекту LayoutManager
        crimeRecyclerView.layoutManager = LinearLayoutManager(context)

        crimeRecyclerView.adapter = adapter

        return view
    }


    /**
     * Fragment.onViewCreated(...) вызывается после возврата Fragment.onCreateView(...),
     * давая понять, что иерархия представления фрагмента находится на месте.
     *
     * Мы наблюдаем за LiveData от onViewCreated(...), чтобы убедиться, что виджет готов
     * к отображению данных о преступлении. Поэтому мы передаем функции observe() объект viewLifecycleOwner,
     * а не сам фрагмент.
     *
     * viewLifecycleOwner - это жизненный цикл представления.
     *
     * Нам нужно получать список преступлений только в том случае,
     * если представление в должном состоянии, и тогда мы не будем получать уведомления,
     * когда виджет не находится на экране.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /**
         * эта функция используется для регистрации наблюдателя за экземпляром LiveData и
         * связи наблюдейния с жизненным циклом другого компонента
         *
         * Observer отвечает за реакцию на новые данные из LiveData - в данном случае,
         * блок кода наблюдателя выполняется всякий раз, когда обновляется список в LiveData
         */
        crimeListViewModel.crimeListLiveData.observe(
            viewLifecycleOwner, { crimes ->
                crimes?.let {
                    Log.i(TAG, "Got crimes ${crimes.size}")
                    updateUI(crimes)
                }
            })
    }

    // Переопределен для отмены свойства callbacks
    override fun onDetach() {
        super.onDetach()
        callbacks = null
    }

    /**
     * Здесь настраиваем интерфейс CrimeListFragment
     */
    private fun updateUI(crimes: List<Crime>) {
        adapter = CrimeAdapter((crimes)) // Создание Адаптера
        crimeRecyclerView.adapter = adapter // Подключение Адаптера на RecyclerView
    }


    /**
     * inner, тем самым он будет иметь доступ к членам внешнего класса.
     * Внутренние классы содержат ссылку на объект внешнего класса
     *
     * RecyclerView ожидает, что элемент представления будет обернут в экзмемпляр ViewHolder/
     * ViewHolder хранит ссылку на представление элемента.
     * RecyclerView никогда не создает объекты View сам по себе. Он всегда создает
     * ViewHolder, которые выводят свои itemView
     */
    private inner class CrimeHolder(view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {

        private lateinit var crime: Crime

        private val titleTextView: TextView = itemView.findViewById(R.id.crime_title)
        private val dateTextView: TextView = itemView.findViewById(R.id.crime_date)
        private val solvedImageView: ImageView = itemView.findViewById(R.id.crime_solved)

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(crime: Crime) {
            this.crime = crime
            titleTextView.text = this.crime.title
            dateTextView.text = this.crime.date.toString()
            solvedImageView.visibility = if (crime.isSolved) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }

        override fun onClick(v: View?) {
            /**
             * Для обработки клика используется интерфейс обратного вызова.
             * При клике на отдельном элементе в списке через интерфейс Callbacks
             * будет удведомляться хост-activity.
             */
            callbacks?.onCrimeSelected(crime.id)
        }
    }

    private inner class CrimeAdapter(var crimes: List<Crime>) :
        RecyclerView.Adapter<CrimeHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CrimeHolder {
            val view = layoutInflater.inflate(R.layout.list_item_crime, parent, false)
            return CrimeHolder(view)
        }

        override fun onBindViewHolder(holder: CrimeHolder, position: Int) {
            val crime = crimes[position]
            holder.bind(crime)
        }

        override fun getItemCount(): Int = crimes.size

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_crime_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.new_crime -> {
                val crime = Crime()
                crimeListViewModel.addCrime(crime)
                callbacks?.onCrimeSelected(crime.id)
                true // Возвращаем true, чтобы показать, что дальнейшая обработка не требуется.
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

}

