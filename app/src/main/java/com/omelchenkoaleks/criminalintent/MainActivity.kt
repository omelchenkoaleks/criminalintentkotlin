package com.omelchenkoaleks.criminalintent

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.omelchenkoaleks.criminalintent.crime.CrimeFragment
import com.omelchenkoaleks.criminalintent.crime_list.CrimeListFragment
import java.util.*

private const val TAG = "MainActivity"

/**
 * В этом приложении Activity будет хостом для фрагментов.
 * Для этого она должна: 1, определить место представления фрагмента в своем макете и
 * 2, управлять жизненным циклом экземпляра фрагмента
 *
 * FragmentManager - работает с двумя вещами: списком фрагментов и обратным стеком транзакций.
 * Он отвечает за добавление фрагментов в иеархию представлений activity и управеление
 * жизненными циклами фрагментов
 */
class MainActivity : AppCompatActivity(), CrimeListFragment.Callbacks {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val currentFragment =
            supportFragmentManager.findFragmentById(R.id.fragment_container)

        if (currentFragment == null) {
            val fragment = CrimeListFragment.newInstance()
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, fragment)
                .commit()
        }
    }

    override fun onCrimeSelected(crimeId: UUID) {
        Log.d(TAG, "MainActivity.onCrimeSelected: $crimeId")
        val fragment = CrimeFragment.newInstance(crimeId)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, fragment) // заменяет фрагмент на другой
            .addToBackStack(null) // Добавление фрагмента в обратный стек
            .commit()
    }

}