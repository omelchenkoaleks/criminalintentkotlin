package com.omelchenkoaleks.criminalintent.database

import androidx.room.TypeConverter
import java.util.*

/**
 * Класс Crime опирается на объекты Date и UUID, которые по умолчанию не знает, как хранить.
 * Базе данных Room нужна помощь чтобы научиться правильно хранить эти типы и извлекать их из
 * таблицы данных.
 * Для этого Room нужно указать преобразователь типов. Этот преобразователь типа сообщает Room, как
 * преобразовать специальный тип в формат для хранения данных в базе и как выполнить обратное
 * преобразование.
 *
 * TypeConverter нужно включить с помощью анотации в RoomDatabase. Так мы показываем Room
 * какие функции использовать при преобразовании типов.
 */
class CrimeTypeConverters {

    @TypeConverter
    fun fromDate(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun toDate(millisSinceEpoch: Long?): Date? {
        return millisSinceEpoch?.let {
            Date(it)
        }
    }

    @TypeConverter
    fun toUUID(uuid: String?): UUID? {
        return UUID.fromString(uuid)
    }

    @TypeConverter
    fun fromUUID(uuid: UUID?): String? {
        return uuid?.toString()
    }

}