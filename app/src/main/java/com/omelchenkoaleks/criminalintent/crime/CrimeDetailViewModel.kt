package com.omelchenkoaleks.criminalintent.crime

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.omelchenkoaleks.criminalintent.CrimeRepository
import com.omelchenkoaleks.criminalintent.model.Crime
import java.io.File
import java.util.*

/**
 * Чтобы при повороте устройства опять не повторять поиск по базе данных добавляем этот
 * ViewModel для управления запросом к базе данных.
 */
class CrimeDetailViewModel : ViewModel() {

    private val crimeRepository = CrimeRepository.get()
    private val crimeIdLiveData = MutableLiveData<UUID>()

    /**
     * Хранит идентификатор отображаемого в данный момент преступления (или выводимого на отображение)
     * фрагментом CrimeFragment.
     */
    var crimeLiveData: LiveData<Crime?> =
        Transformations.switchMap(crimeIdLiveData) { crimeId ->
            crimeRepository.getCrime(crimeId)
        }

    /**
     * CrimeFragment вызовет функцию, чтобы ViewModel понял, какое преступление ему нужно загрузить.
     */
    fun loadCrime(crimeId: UUID) {
        crimeIdLiveData.value = crimeId
    }

    /**
     * Для записи в базу данных значения, которые пользователь вводит на экране детализации.
     */
    fun saveCrime(crime: Crime) {
        crimeRepository.updateCrime(crime)
    }

    fun getPhotoFile(crime: Crime): File {
        return crimeRepository.getPhotoFile(crime)
    }

}