package com.omelchenkoaleks.criminalintent

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import com.omelchenkoaleks.criminalintent.database.CrimeDatabase
import com.omelchenkoaleks.criminalintent.database.migration_1_2
import com.omelchenkoaleks.criminalintent.model.Crime
import java.io.File
import java.util.*
import java.util.concurrent.Executors

/**
 * ВАЖНО:
 * Нужно использовать приватную константу, определенную в том же файле,
 * поскольку никакие другие компоненты не должны получать к ней доступ.
 */
private const val DATABASE_NAME = "crime-database"

/**
 * Repository дает возможность лекгко передавать данные между классами контроллера.
 * Реализован в виде Синглтона - а значит будет существовать до тех пор, пока
 * приложение находится в памяти Андроид. Поэтому хранение в нем любых свойств
 * позволяет получить к ним доступ в течении жизненного цикла активити или фрагмента.
 */
class CrimeRepository private constructor(context: Context) {

    private val filesDir = context.applicationContext.filesDir

    private val database: CrimeDatabase = Room.databaseBuilder(
        context.applicationContext,
        CrimeDatabase::class.java,
        DATABASE_NAME
    ).addMigrations(migration_1_2)
        .build()

    private val crimeDao = database.crimeDao()

    /**
     * Свойство исполнителя для хранения ссылки.
     */
    private val executor = Executors.newSingleThreadExecutor()

    /**
     * Нужно заполнить репозиторий, чтобы остальные компоненты могли выполнять любые операции,
     * необходимые для работы с базой данных. Добавляем функцию в репозиторий для каждой функции в нашем DAO.
     *
     * Поскольку Room обеспечивает реализацию запросов в DAO,
     * мы будем обращаться к этим реализациям из вашего репозитория.
     */
    fun getCrimes(): LiveData<List<Crime>> = crimeDao.getCrimes()

    fun getCrime(id: UUID): LiveData<Crime?> = crimeDao.getCrime(id)

    fun updateCrime(crime: Crime) {
        executor.execute {
            crimeDao.updateCrime(crime)
        }
    }

    fun addCrime(crime: Crime) {
        executor.execute {
            crimeDao.addCrime(crime)
        }
    }

    fun getPhotoFile(crime: Crime): File = File(filesDir, crime.photoFileName)


    /**
     * методы для инициализации и получения объекта CrimeRepository.
     */
    companion object {
        private var INSTANCE: CrimeRepository? = null

        fun initialize(context: Context) {
            if (INSTANCE == null) {
                INSTANCE = CrimeRepository(context)
            }
        }

        fun get(): CrimeRepository {
            return INSTANCE ?: throw IllegalStateException("CrimeRepository must be initialized")
        }
    }

}